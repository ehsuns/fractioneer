#include <string>
#include <ostream>
#include <exception>

class ZeroDenominator: public std::exception {
    public: 
        virtual const char* what() const throw() {
            return "Denominator is 0. =======================================";
        }
};

class Fraction {
    public:
        // constructors
        Fraction();
        Fraction(int numerator, int denominator);
        Fraction(int numerator, int denominator, bool simplifyOnCreate);
        Fraction(const Fraction& copy);
        ~Fraction();

        // operators
        Fraction operator+(const Fraction& rhs) const; 
        Fraction operator-(const Fraction& rhs) const;
        Fraction operator*(const Fraction& rhs) const;
        Fraction operator/(const Fraction& rhs) const;
        Fraction& operator+=(const Fraction& rhs);
        Fraction& operator-=(const Fraction& rhs);
        Fraction& operator*=(const Fraction& rhs);
        Fraction& operator/=(const Fraction& rhs);
        bool operator==(const Fraction& rhs) const;
        bool operator!=(const Fraction& rhs) const;
        bool operator>(const Fraction& rhs) const;
        bool operator>=(const Fraction& rhs) const;
        bool operator<(const Fraction& rhs) const;
        bool operator<=(const Fraction& rhs) const;
        
        friend std::ostream& operator<<(std::ostream& os, const Fraction& f) {
            return os << f._n << "/" << f._d;
        }

        // others
        void simplify();
        Fraction getSimplified() const;

    // Would make these protected normally, but for testing purposes I've left them as public.
    // protected:
        int getNumerator() const;
        int getDenominator() const;
        bool isNegative() const;

    private:
        int _n;     // numerator
        int _d;     // denominator
        int getGcd() const;
        void handleSign();
};