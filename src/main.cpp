#include <stdlib.h>
#include <assert.h>

#include <iostream>
#include <tuple>
#include <random>
#include <functional>
#include "Fraction.hpp"

using namespace std;

// Setup random number generation
std::random_device rd;     // init seed engine
std::mt19937 rng(rd());    // seed an rng
std::uniform_int_distribution<int> uni(-100, 100);     // Limiting range to 100 here to prevent having to deal with rollover

// Test utility functions

Fraction randfrac() {
    // No divides by 0!
    int denominator = 0;
    while (denominator == 0) {
        denominator = uni(rng);
    }

    return Fraction(uni(rng), denominator);
}

double roundTo3(double in) {
    return roundf(in * 1000.0) / 1000.0;
}

double fraceval(const Fraction& f) {
    double multiplied = (double)f.getNumerator() / f.getDenominator();
    return multiplied;
}

// Evaluate fractions to doubles for testing.
tuple<double, double, double> getEvaluatedFractions(const Fraction& result, const Fraction& lhs, const Fraction& rhs) {
    double evalres = fraceval(result);
    double evallhs = fraceval(lhs);
    double evalrhs = fraceval(rhs);

    return make_tuple(evalres, evallhs, evalrhs);
}

// Verify operations by evaluating fractions to doubles and checking equality.
void verifyArithmeticOps(const Fraction& result, const Fraction& lhs, const Fraction& rhs, function<double(double,double)> op) {
    double evalres, evallhs, evalrhs;
    tie (evalres, evallhs, evalrhs) = getEvaluatedFractions(result, lhs, rhs);

    cout << lhs << " OP " << rhs << " == " << result << endl;
    //cout << lhs.display() << " OP " << rhs.display() << " == " << result.display() << endl;
    assert (roundTo3(evalres) == roundTo3(op(evallhs, evalrhs)));
}

void verifyBoolOps(bool result, const Fraction& lhs, const Fraction& rhs, function<bool(double,double)> op) {
    double throwaway, evallhs, evalrhs;
    tie (throwaway, evallhs, evalrhs) = getEvaluatedFractions(Fraction(1,1), lhs, rhs);

    cout << lhs << " OP " << rhs << " == " << result << endl;
    assert (result == op(evallhs, evalrhs));
}

int main() {    

    // Tests Here --------------------------------------------------------------
    int testIterations = 500;
    Fraction f1, f2, tmp, result;
    bool boolResult;

    // Add
    cout << endl << "Runing addition tests... ========================================================" << endl;;
    auto addop = [](double l, double r){ return l + r; };
    for (int i = 0; i < testIterations; ++i) {
        // test operator+
        f1 = randfrac();
        f2 = randfrac();
        result = f1 + f2;
        verifyArithmeticOps(result.getSimplified(), f1, f2, addop);

        // test operator+=
        tmp = result;
        f1 = randfrac();
        result += f1;
        verifyArithmeticOps(result.getSimplified(), tmp, f1, addop);
    }

    // Subtract
    cout << endl << "Runing subtraction tests... ========================================================" << endl;;
    auto subtractop = [](double l, double r){ return l - r; };
    for (int i = 0; i < testIterations; ++i) {
        // test operator-
        f1 = randfrac();
        f2 = randfrac();
        result = f1 - f2;
        verifyArithmeticOps(result.getSimplified(), f1, f2, subtractop);

        // test operator-=
        tmp = result;
        f1 = randfrac();
        result -= f1;
        verifyArithmeticOps(result.getSimplified(), tmp, f1, subtractop);
    }

    // Multiply
    cout << endl << "Running multiplication tests... ========================================================" << endl;
    auto multiplyop = [](double l, double r){ return l * r; };
    for (int i = 0; i < testIterations; ++i) {
        // test operator*
        f1 = randfrac();
        f2 = randfrac();
        result = f1 * f2;
        verifyArithmeticOps(result.getSimplified(), f1, f2, multiplyop);
        
        // test operator*=
        tmp = result;
        f1 = randfrac();
        result *= f1;
        verifyArithmeticOps(result.getSimplified(), tmp, f1, multiplyop);
    }

    // Divide
    cout << endl << "Running division tests... ========================================================" << endl;
    auto divop = [](double l, double r){ return l / r; };
    for (int i = 0; i < testIterations; ++i) {
        // test operator/
        f1 = randfrac();
        f2 = randfrac();
        try {
            result = f1 / f2;
            verifyArithmeticOps(result.getSimplified(), f1, f2, divop);

            // test operator /=
            tmp = result;
            f1 = randfrac();
            result /= f1;
            verifyArithmeticOps(result.getSimplified(), tmp, f1, divop);
        } catch (ZeroDenominator& e) {
            cout << "GOT A DENOMINATOR WITH 0!!!! -- " << e.what() << endl;
        }
    }

    // Equivalence
    cout << endl << "Running equals tests... ========================================================" << endl;
    auto eqop = [](double l, double r){ return l == r; };
    auto gteop = [](double l, double r){ return l >= r; };
    auto lteop = [](double l, double r){ return l <= r; };
    for (int i = 0; i < testIterations; ++i) {
        // test operator==
        int n = uni(rng);
        int d = uni(rng);
        int multiplier = uni(rng);
        try {
            f1 = Fraction(n, d);
            f2 = Fraction(n * multiplier, d * multiplier);
            boolResult = f1 == f2;
            verifyBoolOps(boolResult, f1, f2, eqop);

            // test operation>=
            boolResult = f1 >= f2;
            verifyBoolOps(boolResult, f1, f2, gteop);

            // test operation<=
            boolResult = f1 <= f2;
            verifyBoolOps(boolResult, f1, f2, lteop);
        } catch (ZeroDenominator& e) {
            cout << "GOT A DENOMINATOR WITH 0!!!! -- " << e.what() << endl;
        }
    }

    // Non-equivalenc
    cout << endl << "Running not equals tests... ========================================================" << endl;
    auto neqop = [](double l, double r){ return l != r; };
    for (int i = 0; i < testIterations; ++i) {
        // test operator!=
        f1 = randfrac();
        f2 = randfrac();
        boolResult = f1 != f2;
        verifyBoolOps(boolResult, f1, f2, neqop);
    }

    // Greater-than
    cout << endl << "Running greater-than tests... ========================================================" << endl;
    auto gtop = [](double l, double r){ return l > r; };
    for (int i = 0; i < testIterations; ++i) {
        // test operator>
        f1 = randfrac();
        f2 = randfrac();
        boolResult = f1 > f2;
        verifyBoolOps(boolResult, f1, f2, gtop);

        // test operator>=
        boolResult = f1 >= f2;
        verifyBoolOps(boolResult, f1, f2, gteop);
    }

    // Less-than
    cout << endl << "Running less-than tests... ========================================================" << endl;
    auto ltop = [](double l, double r){ return l < r; };
    for (int i = 0; i < testIterations; ++i) {
        // test operator<
        f1 = randfrac();
        f2 = randfrac();
        boolResult = f1 < f2;
        verifyBoolOps(boolResult, f1, f2, ltop);

        // test operator<=
        boolResult = f1 <= f2;
        verifyBoolOps(boolResult, f1, f2, lteop);
    }

    // -------------------------------------------------------------------------

    return 0;
}