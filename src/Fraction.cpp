#include "Fraction.hpp"
#include <iostream>
#include <sstream>

// PUBLIC SCOPE

// Constructors
Fraction::Fraction() : _n(1), _d(1) {
}

Fraction::Fraction(int numerator, int denominator) : _n(numerator), _d(denominator) {
    // std::cout << "CONSTRUCTING BASIC: " << _n << " / " << _d << std::endl;
    if (_d == 0)
        throw ZeroDenominator();
    handleSign();
}

Fraction::Fraction(int numerator, int denominator, bool simplifyOnCreate) : _n(numerator), _d(denominator) {
    // std::cout << "CONSTRUCTING BASIC simplifyOnCreate: " << _n << " / " << _d << std::endl;
    if (_d == 0)
        throw ZeroDenominator();    handleSign();

    if (simplifyOnCreate)
        simplify();
}

Fraction::Fraction(const Fraction& copy) : _n(copy.getNumerator()), _d(copy.getDenominator()) {
    // std::cout << "CONSTRUCTING COPY: " << _n << " / " << _d << std::endl;
    if (_d == 0)
        throw ZeroDenominator();

    handleSign();
}

Fraction::~Fraction() {
}


// Operators

Fraction Fraction::operator+(const Fraction& rhs) const {
    if (_d == rhs.getDenominator()) {
        // If the denominators are the same, we can just do basic addition of numerators alone.

        return Fraction(_n + rhs.getNumerator(), _d, true);
    }

    // Otherwise, normalize the fractions to have the same denominators and then add numerators
    int newLhsN = (_n * rhs.getDenominator()) + (_d * rhs.getNumerator());
    int newLhsD = (_d * rhs.getDenominator());

    return Fraction(newLhsN, newLhsD, true);
}

Fraction Fraction::operator-(const Fraction& rhs) const {
    if (_d == rhs.getDenominator()) {
        // If the denominators are the same, we can just do basic subtraction of numerators alone.
        return Fraction(_n - rhs.getNumerator(), _d, true);
    }

    // Otherwise, normalize the fractions to have the same denominators and then subtract numerators
    int newLhsN = (_n * rhs.getDenominator()) - (_d * rhs.getNumerator());
    int newLhsD = (_d * rhs.getDenominator());

    return Fraction(newLhsN, newLhsD, true);
}

Fraction Fraction::operator*(const Fraction& rhs) const {
    // With multiplication, we can simply multply numerator with numerator, denominator with denominator, then simplify.
    return Fraction(_n * rhs.getNumerator(), _d * rhs.getDenominator(), true);
}

Fraction Fraction::operator/(const Fraction& rhs) const {
    // Division is the same as multiplying by the reciprocal.
    return Fraction(_n * rhs.getDenominator(), _d * rhs.getNumerator(), true);
}

Fraction& Fraction::operator+=(const Fraction& rhs) {
    if (_d == rhs.getDenominator()) {
        _n += rhs.getNumerator();
        return *this;
    }

    // Because this is a += operation, we have to modify this object's state.
    // Therefore, we normalize to the right-hand side object and modify this object's values.
    auto tempN = _n * rhs.getDenominator();
    auto tempD = _d * rhs.getDenominator();
    auto normRhsN = rhs.getNumerator() * _d;

    _n = tempN + normRhsN;
    _d = tempD;

    simplify();
    return *this;
}

Fraction& Fraction::operator-=(const Fraction& rhs) {
    if (_d == rhs.getDenominator()) {
        _n -= rhs.getNumerator();
        return *this;
    }

    // See comments in operator+=, must normalize to rhs.
    auto tempN = _n * rhs.getDenominator();
    auto tempD = _d * rhs.getDenominator();
    auto normRhsN = rhs.getNumerator() * _d;

    _n = tempN - normRhsN;
    _d = tempD;

    simplify();
    return *this;
}

Fraction& Fraction::operator*=(const Fraction& rhs) {
    _n *= rhs.getNumerator();
    _d *= rhs.getDenominator();

    simplify();
    return *this;
}

Fraction& Fraction::operator/=(const Fraction& rhs) {
    _n *= rhs.getDenominator();
    _d *= rhs.getNumerator();

    simplify();
    return *this;
}

bool Fraction::operator==(const Fraction& rhs) const {
    // Check simple cases first
    if (_n == 0 && rhs.getNumerator() == 0)
        return true;

    if (_n == 0 && rhs.getNumerator() != 0)
        return false;

    if (_n != 0 && rhs.getNumerator() == 0)
        return false;

    // Both numerators are non-zero, so if they are equal, they should simplify to the same thing.
    Fraction lhs = this->getSimplified();
    Fraction newrhs = rhs.getSimplified();

    if (lhs.getNumerator() != newrhs.getNumerator())
        return false;
    
    if (lhs.getDenominator() != newrhs.getDenominator())
        return false;
    
    return true;
}

bool Fraction::operator!=(const Fraction& rhs) const {
    return !(*this == rhs);
}

bool Fraction::operator>(const Fraction& rhs) const {
    // Handle simple cases first
    if (_n == 0 && rhs.getNumerator() ==0)
        return false;

    if (_n == 0) {
        if (rhs.isNegative())
            return true;
        return false;
    }
    
    if (rhs.getNumerator() == 0) {
        if (isNegative())
            return false;
        return true;
    }

    if (!isNegative() && rhs.isNegative())
        return true;
    else if (isNegative() && !rhs.isNegative())
        return false;

    // Ruled out cases where numerator is zero or one fractions is positive while other is not.  Now normalize and compare.
    int normalizedLhsN = _n * rhs.getDenominator();
    int normalizedRhsN = _d * rhs.getNumerator();

    if (normalizedLhsN > normalizedRhsN)
        return true;
    
    return false;
}

bool Fraction::operator>=(const Fraction& rhs) const {
    // Handle simple cases first
    if (_n == 0 && rhs.getNumerator() == 0)
        return true;

    if (_n == 0) {
        if (rhs.isNegative())
            return true;
        return false;
    }
    
    if (rhs.getNumerator() == 0) {
        if (isNegative())
            return false;
        return true;
    }

    if (!isNegative() && rhs.isNegative())
        return true;
    else if (isNegative() && !rhs.isNegative())
        return false;

    // Ruled out cases where numerator is zero or one fractions is positive while other is not.  Now normalize and compare.
    int normalizedLhsN = _n * rhs.getDenominator();
    int normalizedRhsN = _d * rhs.getNumerator();

    if (normalizedLhsN >= normalizedRhsN)
        return true;
    
    return false;
}

bool Fraction::operator<(const Fraction& rhs) const {
    // Since we have the code above, check for the inverse.
    return !(*this >= rhs);
}

bool Fraction::operator<=(const Fraction& rhs) const {
    // Since we have the code above, check for the inverse.
    return !(*this > rhs);
}

// Other public methods

// This method is used when we want to the fraction to be simplified.
void Fraction::simplify() {
    // To simplify fractions, have to find the greatest common dividor b/w numerator and denominator.  
    auto gcd = getGcd();

    // Once we have the GCD, divide both numerator and dividor by it.
    _n /= gcd;
    _d /= gcd;
}

// If you don't want to modify the state of the fraction, but want a simplified copy, use this method.
Fraction Fraction::getSimplified() const {
    auto gcd = getGcd();
    return Fraction(_n / gcd, _d / gcd);
}

int Fraction::getNumerator() const {
    return _n;
}

int Fraction::getDenominator() const {
    return _d;
}

bool Fraction::isNegative() const {
    if (_n == 0)
        return false;

    if (_n < 0 && _d > 0)
        return true;

    if (_n > 0 && _d < 0)
        return true;
    
    return false;
}


// PRIVATE SCOPE

void Fraction::handleSign() {
    // If both are negative, net result is positive, remove both.
    if (_n < 0 && _d < 0) {
        _n *= -1;
        _d *= -1;
    }

    // If only one is negative, move negative sign to numerator, better for printing I think.
    if (_n > 0 && _d < 0) {
        _n *= -1;
        _d *= -1;
    }
}

int Fraction::getGcd() const {
    auto least = std::min(std::abs(_n), std::abs(_d));
    int gcd = 1;
    for (auto i = least; i > 1; --i) {
        if (_n % i == 0 && _d % i == 0) {
            gcd = i;
            break;
        }
    }

    return gcd;
}
