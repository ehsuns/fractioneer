
all: fractioneer.out

fractioneer.out: makeobjsdir objs/main.o objs/Fraction.o
	g++ -o fractioneer.out objs/main.o objs/Fraction.o

makeobjsdir:
	mkdir -p objs/

objs/main.o:
	g++ -o objs/main.o -c src/main.cpp -I ./include

objs/Fraction.o:
	g++ -o objs/Fraction.o -c src/Fraction.cpp -I ./include

clean:
	rm -rf fractioneer.out objs/